<?php

namespace App\Controller;

use App\Repository\PommeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Pomme;


class PommeController extends AbstractController
{
    private $pommeRepository;
//    private $entityManager;

    public function __construct(PommeRepository $pommeRepository )
    {
        $this->pommeRepository =$pommeRepository;

    }

    /**
     * @Route("/pomme", name="pomme")
     */
    public function Pomme()
    {
         $pommeArray= $this->pommeRepository->findAll();
         $nom=$pommeArray->getNomVariete();
          $this->getDoctrine();


//        $pommeArray = $this->get('serializer')->serialize($pommeArray, 'json');
//
//        return new JsonResponse(
//            $pommeArray, 200, [], true);
        return $this->render('pomme/index.html.twig', [
            'controller_name' => 'PommeController',
            'pommeArray'=>$nom,

        ]);
    }
}
